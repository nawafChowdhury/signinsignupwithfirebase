//
//  ViewController.swift
//  signUpandsignInWithFirebase
//
//  Created by as on 1/23/20.
//  Copyright © 2020 as. All rights reserved.
//

import UIKit
import Firebase
import Toaster
import GoogleSignIn



class ViewController: UIViewController ,GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
    
    

    
    @IBOutlet weak var emailTf: UITextField!
    
    
    @IBOutlet weak var passTf: UITextField!
    
    
    
    
   
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.signIn()
        
        let gSignIN = GIDSignInButton(frame: CGRect(x: 0, y: 0, width: 230, height: 40))
        gSignIN.center = view.center
        view.addSubview(gSignIN)

    }

    
    
    
    @IBAction func signUpBtn(_ sender: UIButton) {
        
        
        guard let email = emailTf.text ,  email != "" ,
            let password = passTf.text , password != ""
        
            else{
                
                AlertControl.showAlert(self, title: "error", message: "please fill out all the fields")
                
                
                
                return
                
        }
        
        
        
        Auth.auth().createUser(withEmail: email, password: password){
            user , error in
            
            if user != nil {
                
                
                self.performSegue(withIdentifier: "signInSignUpToHome", sender: nil)
            }
            
            if error != nil{
                AlertControl.showAlert(self, title: "there was an error with making a new user", message: "try again later")
                
                print(error?.localizedDescription)
            }
        }
        
        
        
        
       }
    
    
    
    @IBAction func signInbtn(_ sender: UIButton) {
    
        
        
        guard let email = emailTf.text , email != "",
            let password = passTf.text , password != ""
        
        
        else{
            
            
            AlertControl.showAlert(self, title: "there was an error with signIn", message: "fill out all the fields")
            
            //Toast(text: "this is wrong" , duration: 1.1).show()
            
            return
        }
        
        
        Auth.auth().signIn(withEmail: email, password: password) {
            
            user , error in
            
            
            if user != nil{
                self.performSegue(withIdentifier: "signInSignUpToHome", sender: nil)
            }
            
            if error != nil{
                
                print(error?.localizedDescription)
                
                AlertControl.showAlert(self, title: "there was an error", message: "try again")
                
               print( Toast(text: "wrong").show())
            }
            
        }
    
    }
    
    
    
 
}


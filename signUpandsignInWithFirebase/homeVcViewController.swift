//
//  homeVcViewController.swift
//  signUpandsignInWithFirebase
//
//  Created by as on 1/23/20.
//  Copyright © 2020 as. All rights reserved.
//

import UIKit

import Firebase

class homeVcViewController: UIViewController {

    
    
    @IBOutlet weak var lbl: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        guard  let emailForVc = Auth.auth().currentUser?.email  else {return}
        
        lbl.text = "\(emailForVc)"
    }
    

    
    @IBAction func logoutBtn(_ sender: UIButton) {
        
        
        do{
            try
                Auth.auth().signOut()
             performSegue(withIdentifier: "homeTOSignInSignUP", sender: sender)
            
//            let welcomeScreen = ViewController()
//            let welcomeNavigationController = UINavigationController(rootViewController: welcomeScreen)
//            self.present(welcomeNavigationController, animated: true, completion: nil)
            
            
            
            
        }
        
        catch{
            print(error.localizedDescription)
            
        }
        
        
        
    }
    
    

}

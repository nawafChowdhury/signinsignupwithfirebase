//
//  AlertControl.swift
//  signUpandsignInWithFirebase
//
//  Created by as on 1/23/20.
//  Copyright © 2020 as. All rights reserved.
//

import UIKit

class AlertControl{
    static func showAlert (_ inViewController : UIViewController ,title : String , message : String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        
        let action = UIAlertAction(title: "ok", style: .default, handler: nil)
        
        
        alert.addAction(action)
        
        inViewController.present(alert, animated: true, completion:nil)
        
    }
}
